// ==UserScript==
// @name        MusicBrainz: Fast cancel edits
// @version     2015.6.7.1
// @author      Michael Wiencek
// @downloadURL https://github.com/murdos/musicbrainz-userscripts/raw/master/fast-cancel-edits.user.js
// @updateURL   https://github.com/murdos/musicbrainz-userscripts/raw/master/fast-cancel-edits.user.js
// @include     *://musicbrainz.org/user/*/edits/open*
// @include     *://musicbrainz.org/*/*/open_edits*
// @include     *://musicbrainz.org/*/*/edits*
// @include     *://musicbrainz.org/search/edits*
// @include     *://*.musicbrainz.org/user/*/edits/open*
// @include     *://*.musicbrainz.org/*/*/open_edits*
// @include     *://*.musicbrainz.org/*/*/edits*
// @include     *://*.musicbrainz.org/search/edits*
// @include     *://*.mbsandbox.org/user/*/edits/open*
// @include     *://*.mbsandbox.org/*/*/open_edits*
// @include     *://*.mbsandbox.org/*/*/edits*
// @include     *://*.mbsandbox.org/search/edits*
// @match       *://musicbrainz.org/user/*/edits/open*
// @match       *://musicbrainz.org/*/*/open_edits*
// @match       *://musicbrainz.org/*/*/edits*
// @match       *://musicbrainz.org/search/edits*
// @match       *://*.musicbrainz.org/user/*/edits/open*
// @match       *://*.musicbrainz.org/*/*/open_edits*
// @match       *://*.musicbrainz.org/*/*/edits*
// @match       *://*.musicbrainz.org/search/edits*
// @match       *://*.mbsandbox.org/user/*/edits/open*
// @match       *://*.mbsandbox.org/*/*/open_edits*
// @match       *://*.mbsandbox.org/*/*/edits*
// @match       *://*.mbsandbox.org/search/edits*
// @grant       none
// ==/UserScript==
//**************************************************************************//

var scr = document.createElement("script");
scr.textContent = "(" + fastCancelScript + ")();";
document.body.appendChild(scr);

function fastCancelScript() {
    var totalCancels = 0;

    var $status = $("<div></div>")
        .css({
            "position": "fixed",
            "right": "0",
            "bottom": "0",
            "background": "#FFBA58",
            "border-top": "1px #000 solid",
            "border-left": "1px #000 solid",
            "padding": "0.5em"
        })
        .appendTo("body")
        .hide();

    function updateStatus() {
        if (totalCancels === 0) {
            $status.hide();
        } else {
            $status.text("Canceling " + totalCancels + " edit" +
                (totalCancels > 1 ? "s" : "") + "...").show();
        }
    }

    document.body.addEventListener("click", function (event) {
        if (event.target && event.target.tagName && event.target.tagName == "A" && event.target.classList.contains("negative")) {
            event.stopPropagation();
            event.preventDefault();
            totalCancels += 1;
            updateStatus();

            var $self = $(event.target),
                $edit = $self.parents("div.edit-list:eq(0)");

            pushRequest(function () {
                var editNote = $edit.find("div.add-edit-note textarea").val();
                var data = { "confirm.edit_note": editNote };

                $.ajax({
                    type: "POST",
                    url: $self.attr("href"),
                    data: data,
                    error: function (request, status, error) {
                        $self
                            .css({
                                "background": "red",
                                "color": "yellow",
                                "cursor": "help"
                            })
                            .attr("title", "Error cancelling this edit: “" + error + "”");
                        $edit
                            .css({border: "6px solid red"})
                            .show();
                    },
                    complete: function () {
                        $edit.remove();
                        totalCancels -= 1;
                        updateStatus();
                    }
                });
            });
            $edit.hide();
        }
    });

    $("div#edits > form[action$='/edit/enter_votes']").on("submit", function(event) {
    	if (totalCancels > 0) {
    		event.preventDefault();
    		alert("Please wait, " + (totalCancels > 1 ? totalCancels + " edits are" : "an edit is") + " being cancelled in the background.");
    	}
    });

    var pushRequest = (function () {
        var queue = [],
            last = 0,
            active = false,
            rate = 2000;

        function next() {
            if (queue.length === 0) {
                active = false;
            } else {
                queue.shift()();
                last = new Date().getTime();
                setTimeout(next, rate);
            }
        }

        return function (req) {
            queue.push(req);

            if (!active) {
                active = true;
                var now = new Date().getTime();
                if (now - last >= rate) {
                    next();
                } else {
                    var timeout = rate - now + last;
                    setTimeout(next, timeout);
                }
            }
        };
    }());
}
